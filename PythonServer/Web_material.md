# Websocket
[Websockets vs REST](https://ably.com/topic/websocket-vs-rest#:~:text=WebSockets%20have%20a%20low%20overhead,%2C%20delete%2C%20or%20update%20resources.)
[Synchronous vs Asynchornous in Python](https://blog.miguelgrinberg.com/post/sync-vs-async-python-what-is-the-difference)
Use Channels or SocketIO
1. Django Channels
[Django Channels Documentations](https://channels.readthedocs.io/en/stable/)
[Django Websocket Beginners Guide](https://esketchers.com/django-websockets-a-complete-beginners-guide/#:~:text=Django%202.0%20or%20above%20comes,need%20for%20WebSockets%20in%20Django.)
2. SocketIO
[Django SocketIO](https://pypi.org/project/django-socketio/)
[How to use Python Socket IO with Django](https://studygyaan.com/django/how-to-use-python-socket-io-with-django)