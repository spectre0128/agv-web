import Dto.MapTopology
import Dto.Road
import Bll.Road
import Dto.AGVCar

class Road:
    @staticmethod
    def GetDistance(PreviousNode, NextNode):
        return float(Dto.MapTopology.MapTopology.Map[int(PreviousNode)][int(NextNode)])
    
    def GetDirection(PreviousNode, NextNode):
        return int(Dto.MapTopology.MapTopology.Direction[int(PreviousNode)][int(NextNode)])
 
    @staticmethod
    def returnListOfRoad(ListOfNode):
        ListOfRoad = list()
        for i in range(0,len(ListOfNode)-1):
            Road = Dto.Road.Road(ListOfNode[i],ListOfNode[i+1],Bll.Road.Road.GetDistance(ListOfNode[i],ListOfNode[i+1]), Bll.Road.Road.GetDirection(ListOfNode[i],ListOfNode[i+1]))
            ListOfRoad.append(Road)
        return ListOfRoad
        