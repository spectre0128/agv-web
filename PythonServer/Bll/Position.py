import Dto.Schedule
import Dto.Road
import Dto.Position
import Dto.ControlSignal
import Bll.Convert

class Position:
    @staticmethod 
    def returnPosition(Time,Schedule):
        Position = Dto.Position.Position()
        TimeCounter = Bll.Convert.Convert.TimeToTimeStamp(Schedule.TimeStart)
        Index = int(0)
        TimeEnd = Bll.Convert.Convert.TimeToTimeStamp(Schedule.TimeEnd)
        while(TimeCounter <= TimeEnd):
            Distance = Schedule.ListOfControlSignal[Index].Road.Distance
            Velocity = Schedule.ListOfControlSignal[Index].Velocity
            if(TimeCounter + Distance/Velocity > Time):
                Position.FirstNode = Schedule.ListOfControlSignal[Index].Road.FirstNode
                Position.SecondNode = Schedule.ListOfControlSignal[Index].Road.SecondNode
                Position.TravelledDistance = float(Velocity)*(Time - TimeCounter)
                Position.Car = Schedule.Car
                Position.Velocity = Schedule.Car
                return Position
            Index = Index + int(1)
            if(Index == len(Schedule.ListOfControlSignal)):
                break
            TimeCounter = TimeCounter + Distance/Velocity 
        return Position
            