import Dto.Road
import Bll.Road
import Dto.AGVCar
import Bll.Constrains
class ControlSignal:
    def returnListOfControlSignal(TimeStart,ListOfNode):
        ListOfControlSignal = list()
        ListOfRoad = Bll.Road.Road.returnListOfRoad(ListOfNode)
        for EachRoad in ListOfRoad:
            ListOfControlSignal.append(Bll.Constrains.Constrains.CollisionConstrain(TimeStart,EachRoad))
        for Element in ListOfControlSignal:
            if(Element.Velocity >= Dto.AGVCar.AGVCar.MaxVelocity):
                Element.Velocity = Dto.AGVCar.AGVCar.MaxVelocity
            elif(Element.Velocity >= 0.20):
                Element.Velocity = 0.20
            elif(Element.Velocity >= 0.15):
                Element.Velocity = 0.15
            elif(Element.Velocity >= 0.1):
                Element.Velocity = 0.1
            else:
                Element.Velocity = 0.1
        return ListOfControlSignal
