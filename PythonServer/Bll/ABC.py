import Bll.MapExecution
import Dto.Population
import Dto.ABCParameter
import numpy
import Bll.CostFunction
class ABC:
    def __init__(self):
        self.BestCost = Dto.Population.Population()
        self.ListOfPop = list()
        self.ListOfFitnessValue = [0]*Dto.ABCParameter.ABCSetting.nPop
        self.ListOfProbability = [0]*Dto.ABCParameter.ABCSetting.nPop
        self.BestCostList = list()
        
    @staticmethod
    def returnCostFunction(ListOfControlSignal,Outbound,LoadWeight):
        CostFunctionValue = float(0)
        
        if(int(ListOfControlSignal[len(ListOfControlSignal)-int(1)].Road.SecondNode) != int(Outbound)):
            return float(100000)
        else:    
            CostFunctionValue = Bll.CostFunction.CostFunction.returnCostFunction(ListOfControlSignal,LoadWeight)
        return CostFunctionValue
    
    @staticmethod
    def CreateInitialPopulation(self,Inbound,Outbound,LoadWeigth,TimeStart):
        NumberOfPopulation = Dto.ABCParameter.ABCSetting.nPop 
        for i in range (NumberOfPopulation):
            NewPop = Dto.Population.Population(Inbound,Outbound,LoadWeigth,TimeStart)
            if(self.BestCost.CostValue >= NewPop.CostValue):
                self.BestCostList.append(self.BestCost.CostValue)
                self.BestCost = NewPop
            self.ListOfPop.append(NewPop)
    
    @staticmethod
    def RecruitedBees(self):
        NumberOfPopulation = Dto.ABCParameter.ABCSetting.nPop 
        
        for i in range (NumberOfPopulation):
            # New Bee Generation
            NewBee = Dto.Population.Population()                
            # Comparision
            if (NewBee.CostValue <= self.ListOfPop[i].CostValue):
                self.ListOfPop[i].CostValue = NewBee.CostValue
            else:
                self.ListOfPop[i].AbandonmentCounter = self.ListOfPop[i].AbandonmentCounter + 1
    
    @staticmethod
    # Calculate Fitness Value
    def CalculateFitness(self):
        SumOfFitness = float(0)
        NumberOfPopulation = Dto.ABCParameter.ABCSetting.nPop
        for i in range(NumberOfPopulation):
            # Calculate fitness value
            FitnessValue = 1/float(1+self.ListOfPop[i].CostValue)
            self.ListOfFitnessValue[i] = FitnessValue 
            SumOfFitness = SumOfFitness + FitnessValue
        
        for i in range(NumberOfPopulation):
            #Calculate Probability
            self.ListOfProbability[i] = self.ListOfFitnessValue[i]/SumOfFitness
       
    @staticmethod     
    def RouletteWheelSelection(self):
        RandomNumber = numpy.random.uniform(0,1)
        Probability = self.ListOfProbability[0]
        for i in range (len(self.ListOfProbability)):
            if(RandomNumber < Probability):
                return int(i)
            else:
                Probability = Probability + self.ListOfProbability[i]
                
    @staticmethod
    def OnlookerBees(self):
        NumberOfOnlooker = Dto.ABCParameter.ABCSetting.nOnlooker
        for i  in range (NumberOfOnlooker):
            # Select Source Site
            Source = self.RouletteWheelSelection(self)

            # New Bee Generation
            NewBee = Dto.Population.Population() 
            
            # Comparision
            if (NewBee.CostValue <= self.ListOfPop[i].CostValue):
                self.ListOfPop[i] = NewBee
            else:
                self.ListOfPop[i].AbandonmentCounter = self.ListOfPop[i].AbandonmentCounter + 1
    
    @staticmethod
    def ScoutBees(self):
        NumberOfPopulation = Dto.ABCParameter.ABCSetting.nPop
        for i in range (NumberOfPopulation):
            if (self.ListOfPop[i].AbandonmentCounter >= Dto.ABCParameter.ABCSetting.L):
                self.ListOfPop[i] = Dto.Population.Population()
                self.ListOfPop[i].AbandonmentCounter = 0
    
    @staticmethod
    # Update Best Solution Ever Found
    def BestSolution(self):
        NumberOfPopulation = Dto.ABCParameter.ABCSetting.nPop
        for i in range (NumberOfPopulation):
            if (self.ListOfPop[i].CostValue <= self.BestCost.CostValue):
                self.BestCost = self.ListOfPop[i]
                self.BestCostList.append(self.BestCost.CostValue)
    
    @staticmethod
    def ABCAlgorithm(self,Inbound,Outbound,LoadWeight,TimeStart):
        for i in range(Dto.ABCParameter.ABCSetting.MaxIt):
            self.CreateInitialPopulation(self,Inbound,Outbound,LoadWeight,TimeStart)
            self.RecruitedBees(self)
            self.CalculateFitness(self)
            self.OnlookerBees(self)
            self.ScoutBees(self)
            self.BestSolution(self)
        return self.BestCost
            