import Dto.Schedule
import Dto.Road
import Bll.Position
import Dto.AGVCar
import Dto.ControlSignal
import Bll.Road
import Bll.Convert

class Constrains:
    def CollisionConstrain(TimeStart,Road):
        Test = Bll.Convert.Convert.returnTimeStampToTime(TimeStart)
        
        ControlSignal = Dto.ControlSignal.ControlSignal(Road)
        if(len(Dto.Schedule.Schedule.ListOfSchedule) > 0):
            for EachSchedule in Dto.Schedule.Schedule.ListOfSchedule:
                returnPosition = Bll.Position.Position.returnPosition(TimeStart,EachSchedule)
                if(returnPosition.FirstNode != ""):
                    # Path constrain
                    if(Road.FirstNode == returnPosition.SecondNode and Road.SecondNode == returnPosition.FirstNode):
                        ControlSignal.Road = Dto.Road.Road(0,0,100000)
                        ControlSignal.Velocity = Dto.AGVCar.AGVCar.MaxVelocity
                        return ControlSignal
                    # Node constrain  
                    # Similar direction
                    ControlSignal.Road = Road
                    if(Road.FirstNode == returnPosition.FirstNode and returnPosition.TravelledDistance < Dto.AGVCar.AGVCar.SafetyDistance):
                        ControlSignal.Velocity = float(returnPosition.TravelledDistance)/(float(Dto.AGVCar.AGVCar.SafetyDistance)*Dto.AGVCar.AGVCar.MaxVelocity)
                        continue

                TimeEnd = TimeStart + Road.Distance/ControlSignal.Velocity
                # Middle point
                returnPosition = Bll.Position.Position.returnPosition(TimeStart + (TimeEnd - TimeStart)/2,EachSchedule)
                if(returnPosition.FirstNode != ""):
                    # Path constrain
                    if(Road.FirstNode == returnPosition.SecondNode and Road.SecondNode == returnPosition.FirstNode):
                        ControlSignal.Road = Dto.Road.Road(0,0,100000)
                        ControlSignal.Velocity = Dto.AGVCar.AGVCar.MaxVelocity
                        return ControlSignal

                # End point
                returnPosition = Bll.Position.Position.returnPosition(TimeEnd-0.1,EachSchedule)
                if(returnPosition.FirstNode != ""):
                    # Path constrain
                    if(Road.FirstNode == returnPosition.SecondNode and Road.SecondNode == returnPosition.FirstNode):
                        ControlSignal.Road = Dto.Road.Road(0,0,100000)
                        ControlSignal.Velocity = Dto.AGVCar.AGVCar.MaxVelocity
                        return ControlSignal
                # Node constrain  
                # Similar direction
                ControlSignal.Road = Road
                if(Road.SecondNode == returnPosition.SecondNode and  Road.Distance - returnPosition.TravelledDistance  < Dto.AGVCar.AGVCar.SafetyDistance):
                    ControlSignal.Velocity = round(Road.Distance/(float(Road.Distance - returnPosition.TravelledDistance + Dto.AGVCar.AGVCar.SafetyDistance)/Dto.AGVCar.AGVCar.MaxVelocity + float(Road.Distance)/Dto.AGVCar.AGVCar.MaxVelocity),2)
                    continue
                # Different direction
                if(Road.SecondNode == returnPosition.FirstNode and (returnPosition.TravelledDistance < Dto.AGVCar.AGVCar.SafetyDistance)):
                    ControlSignal.Velocity  = ControlSignal.Road.Distance*ControlSignal.Velocity/(Dto.AGVCar.AGVCar.SafetyDistance - returnPosition.TravelledDistance + ControlSignal.Road.Distance)
                    continue

        return ControlSignal