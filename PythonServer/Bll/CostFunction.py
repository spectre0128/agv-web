import Bll.MapExecution
import Bll.Constrains
import Bll.ControlSignal
import Bll.EnergyConsumption
class CostFunction:
    def returnCostFunction(ListOfControlSignal,LoadWeight):
        CostValue = Bll.EnergyConsumption.EnergyConsumption.returnToTalEnergy(ListOfControlSignal,LoadWeight)
        return CostValue