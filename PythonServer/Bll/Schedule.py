import Dto.Schedule
import Dto.AGVCar
import Bll.Convert
import Dto.AGVCar
import Bll.ABC
import Bll.Requirement
import Bll.CarSelection
import Bll.Schedule
class Schedule:
    def returnSchedule(Requirement,SelectedCarTrip,SelectedTransportingTrip):
        Schedule = Dto.Schedule.Schedule()
        Schedule.Name = Requirement.Name
        Schedule.Order = Requirement.Order
        Schedule.Date = Requirement.Date
        Schedule.Car = SelectedCarTrip.Car
        Schedule.Car.Location = Requirement.Outbound
        Schedule.Inbound = Requirement.Inbound
        Schedule.Outbound = Requirement.Outbound
        Schedule.TimeStart = Requirement.TimeStart
        Schedule.LoadWeight = Requirement.LoadWeight
        Schedule.ListOfControlSignal = SelectedCarTrip.Cost.ListOfControlSignal + SelectedTransportingTrip.ListOfControlSignal
        Schedule.TimeEnd = Bll.Convert.Convert.returnTimeStampToTime(Bll.Convert.Convert.TimeToTimeStamp(Schedule.TimeStart) + Bll.Convert.Convert.returnScheduleToTravellingTime(Schedule.ListOfControlSignal) + Dto.AGVCar.AGVCar.delayTime)
        Schedule.TotalEnergy = round(SelectedCarTrip.Cost.CostValue + SelectedTransportingTrip.CostValue,3)
        Schedule.Car.BatteryCapacity = round((float(Schedule.Car.BatteryCapacity)*Dto.AGVCar.AGVCar.MaxBatteryCapacity/100 - float(Schedule.TotalEnergy))*float(100)/(Dto.AGVCar.AGVCar.MaxBatteryCapacity),2)
        Schedule.BatteryCapacity = Schedule.Car.BatteryCapacity
        Schedule.Car.ScheduleList.append(Schedule)
        return Schedule
    
    def returnListOfSchedule():
        ListOfRequirement = Bll.Requirement.Requirement.ReadTimeTable()
        NewABC = Bll.ABC.ABC()
        Bll.CarSelection.CarSelection.InitialCar()
        for EachRequirement in ListOfRequirement:
            NewABC = Bll.ABC.ABC()
            SelectedCarTrip = Bll.CarSelection.CarSelection.returnSelectedCar(EachRequirement)
            TimeStart = Bll.Convert.Convert.TimeToTimeStamp(EachRequirement.TimeStart)
            if(len(SelectedCarTrip.Cost.ListOfControlSignal) > 1):
                TimeStart = Bll.Convert.Convert.returnScheduleToTravellingTime(SelectedCarTrip.Cost.ListOfControlSignal) + TimeStart
            SelectedTransportingTrip = NewABC.ABCAlgorithm(NewABC,EachRequirement.Inbound,EachRequirement.Outbound,EachRequirement.LoadWeight,TimeStart)
            Schedule = Bll.Schedule.Schedule.returnSchedule(EachRequirement,SelectedCarTrip,SelectedTransportingTrip)
            Dto.Schedule.Schedule.ListOfSchedule.append(Schedule)

    def return_to_lot(startNode, stopNode, loadWeight, timeStart):
        NewABC = Bll.ABC.ABC()
        Route = list()
        Route = NewABC.ABCAlgorithm(NewABC,startNode,stopNode, loadWeight, timeStart)
        return Route
