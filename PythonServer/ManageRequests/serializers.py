from rest_framework import serializers
from .models import DB_SimpleOrder, DB_Schedule

class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = DB_SimpleOrder
        fields = ('request_id', 'order_number', 'order_date', 'load_name', 'load_amount', 'start_time', 'from_node', 'to_node')
        read_only_fields = ('load_weight',)

class ScheduleSerializer(serializers.ModelSerializer):
    class Meta:
        model = DB_Schedule
        fields = ('id', 'order_number', 'order_date', 'load_name', 'from_node', 'load_weight', 'car_id', 'start_time', 'end_time', 'from_node', 'to_node', 'control_signal', 'completion_st')

class CreateListModelMixin(object):
    def get_serializer(self, *args, **kwargs):
        """ if an array is passed, set serializer to many """
        if isinstance(kwargs.get('data', {}), list):
            kwargs['many'] = True
        return super(CreateListModelMixin, self).get_serializer(*args, **kwargs)
    