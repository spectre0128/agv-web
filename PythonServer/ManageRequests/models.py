from django.db import models

from django.utils import timezone

from ManageMaterial.models import DB_Material

# Create your models here.

class DB_SimpleOrder(models.Model):
    request_id = models.BigAutoField(primary_key=True)
    order_date = models.DateField()
    order_number = models.IntegerField(default= 0)
    load_name = models.ForeignKey(DB_Material, to_field='material_name',  on_delete= models.CASCADE, null= True)
    load_amount = models.PositiveIntegerField(default= 0)
    load_weight = models.IntegerField(default= 0)
    start_time = models.CharField(max_length=16, default= '00:00:00')
    from_node = models.PositiveBigIntegerField(default= 0)
    to_node = models.PositiveBigIntegerField(default= 0)
    scheduled_st = models.BooleanField(default=False)
    completion_st = models.BooleanField(default=False)

# DB_Material.objects.filter(material_name = load_name).values_list('material_weight')
    def save(self, *args, **kwargs):
        self.load_weight = DB_Material.objects.get(material_name = self.load_name).material_weight
        super(DB_SimpleOrder, self).save(*args, **kwargs) # Call the "real" save() method.

    def __str__(self):
        return "Simple request #{ID}".format(ID = self.order_number)


class DB_Schedule(models.Model):
    id = models.BigAutoField(primary_key=True)
    order_number = models.IntegerField(default= 0)
    order_date = models.DateField()
    load_name = models.CharField(max_length=16, default= 'none')
    load_weight = models.IntegerField(default= 0)
    car_id = models.IntegerField(default= 0)
    energy_sum = models.IntegerField()
    distance_sum = models.FloatField()
    start_time = models.CharField(max_length=16, default= '00:00:00')
    end_time = models.CharField(max_length=16, default= '00:00:00')
    from_node = models.IntegerField(default= 0)
    to_node = models.IntegerField(default= 0)
    control_signal = models.CharField(max_length=1024, default= '')
    completion_st = models.BooleanField(default=False)

    def __str__(self):
        return "Order #{ID}".format(ID = self.order_number)











# class DB_RequestData(models.Model): 
#     REQUEST_TYPE = (
#         ('AUTO', 'Automatic'),
#         ('MAN', 'Manual')
#     )
#     ACTION_TYPE = (
#         ('NONE', 'No action'),
#         ('PICK', 'Pick up material'),
#         ('DROP', 'Drop material'),
#         ('CHRG', 'Go to charging station')
#     )

#     request_id = models.BigAutoField(primary_key=True)
#     time_stamp = models.DateTimeField(blank= True)
#     request_type = models.CharField(max_length=64, default= 'Automatic', choices= REQUEST_TYPE)
#     action = models.CharField(max_length=64, default= 'No action', choices= ACTION_TYPE)
#     inbound = models.IntegerField(default= 0)
#     outbound = models.IntegerField(default= 0)
#     material = models.CharField(max_length= 255, default= 'None')
#     load_amount = models.IntegerField(default= 0)

#     def __str__(self):
#         return "Request ID: {ID}".format(ID = self.request_id)

    
# class DB_TripData(models.Model): 
#     trip_id = models.BigAutoField(primary_key=True)
#     time_stamp = models.DateTimeField(blank= True)
#     request_type = models.CharField(max_length=64, default= 'Automatic')
#     action = models.CharField(max_length=64, default= 'No action')
#     inbound = models.IntegerField(default= 0)
#     outbound = models.IntegerField(default= 0)
#     material = models.CharField(max_length= 255, default= 'None')
#     load_amount = models.IntegerField(default= 0)

#     def __str__(self):
#         return "Trip ID: {ID}".format(ID = self.trip_id)
