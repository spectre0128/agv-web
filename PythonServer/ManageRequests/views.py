from django.shortcuts import render
from rest_framework import viewsets, generics, status
from django.http import HttpResponse

from .serializers import OrderSerializer, ScheduleSerializer, CreateListModelMixin
from .models import DB_SimpleOrder, DB_Schedule
from ManageRequests.schedule import schedule_agv

# Create your views here.

class OrderView(viewsets.ModelViewSet):
    serializer_class = OrderSerializer
    queryset = DB_SimpleOrder.objects.all()

class ScheduleView(viewsets.ModelViewSet):
    serializer_class = ScheduleSerializer
    queryset = DB_Schedule.objects.all()

class SendTaskView(CreateListModelMixin, generics.CreateAPIView):
    serializer_class = OrderSerializer

def request_schedule(request):
    response = HttpResponse()
    if request.method == 'GET':
        schedule_agv()
        response = HttpResponse("Schedule created successfully!")
    else:
        pass
    return response