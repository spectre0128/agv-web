from django.apps import AppConfig


class ManagerequestsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ManageRequests'
