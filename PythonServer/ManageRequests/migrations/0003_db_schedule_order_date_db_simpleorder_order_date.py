# Generated by Django 4.0 on 2023-05-19 05:38

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('ManageRequests', '0002_alter_db_simpleorder_load_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='db_schedule',
            name='order_date',
            field=models.DateField(default=django.utils.timezone.now),
        ),
        migrations.AddField(
            model_name='db_simpleorder',
            name='order_date',
            field=models.DateField(default=django.utils.timezone.now),
        ),
    ]
