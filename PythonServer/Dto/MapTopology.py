import Bll.MapExecution

class MapTopology:
    Map = Bll.MapExecution.Map.returnMap()[0]
    Direction = Bll.MapExecution.Map.returnMap()[1]
    def __init__(self):
        self.FeasiblePathFactor = Bll.MapExecution.Map.returnFeasiblePathFactor(self.Map)