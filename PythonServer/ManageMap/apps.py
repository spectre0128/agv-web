from django.apps import AppConfig


class ManagemapConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ManageMap'
