from django.shortcuts import render
from rest_framework import viewsets

from .serializers import MaterialSerializer
from .models import DB_Material

# Create your views here.

class MaterialView(viewsets.ModelViewSet):
    serializer_class = MaterialSerializer
    queryset = DB_Material.objects.all()