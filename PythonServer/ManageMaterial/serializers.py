from rest_framework import serializers
from .models import DB_Material

class MaterialSerializer(serializers.ModelSerializer):
    class Meta:
        model = DB_Material
        fields = ('material_id', 'material_name', 'material_unit', 'material_weight',)