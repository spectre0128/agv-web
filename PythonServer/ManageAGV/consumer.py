from channels.generic.websocket import AsyncWebsocketConsumer
from asgiref.sync import async_to_sync
from channels.db import database_sync_to_async
from channels.exceptions import (
    AcceptConnection,
    DenyConnection,
    InvalidChannelLayerError,
    StopConsumer,
)

from django.core import serializers

from .models import DB_AGVData
from .activeAGV import list_active_AGV

from time import sleep

class AGVDataConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        await self.accept()
        while True: 
            jsonSet = await self.jsonize_active_agv()
            await self.send(jsonSet)
            sleep(0.5)

    async def websocket_disconnect(self, message):
        """
        Called when a WebSocket connection is closed. Base level so you don't
        need to call super() all the time.
        """
        await self.disconnect(message["code"])
        raise StopConsumer()

    async def disconnect(self, code):
        """
        Called when a WebSocket connection is closed.
        """
        pass

    @database_sync_to_async
    def jsonize_active_agv(what):
        queryList = []
        listOfAGV = []
        listOfAGV = list_active_AGV()
        
        for eachCar in listOfAGV:
            query = DB_AGVData.objects.filter(car_id = eachCar[0]).last()
            if query:
                queryList.append(query)
            else:
                pass

        queryFields = ('car_id', 'car_state', 'car_battery_capacity', 'car_speed', 'previous_node')
        jsonSet = serializers.serialize('json', queryset=queryList, fields = queryFields)

        return jsonSet