from django.shortcuts import render
from django.http import HttpResponse
from rest_framework import viewsets
from .serializers import AGVSpecsSerializer
from .models import DB_AGVSpecs

# Create your views here.

def index(request):
    html = "<html><body>ManageAGV.</body></html>"
    return HttpResponse(html)

class AGVSpecsView(viewsets.ModelViewSet):
    serializer_class = AGVSpecsSerializer
    queryset = DB_AGVSpecs.objects.all()

