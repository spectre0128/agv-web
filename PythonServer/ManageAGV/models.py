from django.db import models
from PythonWeb.Decode import buffer
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

# Create your models here.

class DB_AGVSpecs(models.Model):
   
    GUIDANCE_TYPE = (
        ('Optical Tape', 'Optical Tape'),
        ('Magnetic Tape', 'Magnetic Tape'),
        ('Wire Guided', 'Wire Guided'),
        ('Laser Guided', 'Laser Guided')
    )
    LOAD_TRANSFER = (
        ('Automatic', 'Automatic'),
        ('Manual ', 'Manual')
    )

    vehicle_id = models.IntegerField(primary_key= True, blank= False)
    vehicle_model = models.CharField(max_length=255, blank=True)
    parking_lot = models.IntegerField(default= 0, blank= False)
    battery_capacity = models.IntegerField(default= 0)
    load_capacity = models.IntegerField(default= 0)
    travel_speed = models.IntegerField(default= 0)
    guidance_type = models.CharField(max_length=255, blank=True, choices= GUIDANCE_TYPE)
    load_transfer = models.CharField(max_length=255, blank=True, choices= LOAD_TRANSFER)
    is_active = models.BooleanField(default= True)
    connected = models.BooleanField(default= True)

    def __str__(self):
        return "Vehicle ID: {ID}".format(ID = self.vehicle_id) + ". " + "Is active: {state}".format(state = self.is_active) + ". " + "Connected: {state}".format(state = self.connected)
    
class DB_AGVStates(models.Model):
    state_id = models.IntegerField(default= 0)
    state_name = models.CharField(max_length= 16, default= 'None')

    def __str__(self):
        return  "State #: {ID}".format(ID = self.state_id) + ": " + "{state}".format(state = self.state_name)

class AGVData():
    class Position():
        def __init__(self, pNode, nNode, distance):
            self.prevNode = pNode
            self.nextNode = nNode
            self.distance = distance

    messageFrameAGVData = [1, 1, 1, 2, 1, 2, 1, 2, 2, 2, 2, 2, 2, 1]
    payloadAGVData = []
    bufferAGVData = []
    carPosition = Position

    def __init__(self, payload):
        self.payloadAGVData = payload
    
    def decodeBuffer(self):
        self.bufferAGVData = buffer.spliceBuffer(self.messageFrameAGVData, self.payloadAGVData)
        self.carID = int.from_bytes(self.bufferAGVData[3], byteorder= 'little')
        self.carState = int.from_bytes(self.bufferAGVData[4], byteorder= 'little')
        self.carBatteryCap = int.from_bytes(self.bufferAGVData[5], byteorder= 'little')
        self.carSpeed = int.from_bytes(self.bufferAGVData[6], byteorder= 'little')
        self.carPosition.prevNode = int.from_bytes(self.bufferAGVData[7], byteorder= 'little')
        self.carPosition.nextNode = int.from_bytes(self.bufferAGVData[8], byteorder= 'little')
        self.carPosition.distance = int.from_bytes(self.bufferAGVData[9], byteorder= 'little')   
        self.energySum = int.from_bytes(self.bufferAGVData[10], byteorder= 'little')
        self.distanceSum = int.from_bytes(self.bufferAGVData[11], byteorder= 'little')
        self.checkSum = int.from_bytes(self.bufferAGVData[11], byteorder= 'little')
    
    def printOut(self):
        print("carId:", self.carID, "state:", self.carState, "battery capacity:", self.carBatteryCap/100, "speed:", self.carSpeed/100, "current position:",
                    self.carPosition.prevNode, self.carPosition.nextNode, self.carPosition.distance/100, "total energy:", self.energySum, "total distance:", self.distanceSum/100)

    def check_sum(self):
        checkSumValue = self.carID + self.carState + self.carBatteryCap + self.carSpeed + self.carPosition.prevNode + self.carPosition.nextNode + self.carPosition.distance + self.energySum + self.distanceSum + self.checkSum
        if (checkSumValue + self.check_sum == 65536):
            return True # packet valid
        else:
            return False # packet invalid

class DB_AGVData(models.Model): 
    data_id = models.BigAutoField(primary_key=True)
    car_id = models.IntegerField()
    car_state = models.IntegerField()
    car_battery_capacity = models.FloatField()
    car_speed = models.FloatField()
    car_position = AGVData.Position
    previous_node = models.IntegerField()
    next_node = models.IntegerField()
    distance = models.FloatField()
    energy_sum = models.IntegerField()
    distance_sum = models.FloatField()
    time_stamp = models.DateTimeField(blank= True)

    def __str__(self):
        return "Data ID: {ID}".format(ID = self.data_id)
    
class AGVError():
    messageFrameAGVError = [1, 1, 1, 2, 1, 1, 2, 2, 1]
    payloadAGVError = []
    bufferAGVError = []

    def __init__(self, payload):
        self.payloadAGVError = payload
    
    def decodeBuffer(self):
        self.bufferAGVError = buffer.spliceBuffer(self.messageFrameAGVError, self.payloadAGVError)
        self.carID = int.from_bytes(self.bufferAGVError[3], byteorder= 'little')
        self.errorCode = int.from_bytes(self.bufferAGVError[4], byteorder= 'little')
        self.orderNum = int.from_bytes(self.bufferAGVError[5], byteorder= 'little')
        self.prevNode = int.from_bytes(self.bufferAGVError[6], byteorder= 'little')
        self.nextNode = int.from_bytes(self.bufferAGVError[7], byteorder= 'little')

class DB_Errors(models.Model):
    id = models.BigAutoField(primary_key=True)
    error_id = models.IntegerField(default= 0, unique= True)
    error_msg = models.CharField(max_length=16, default='')

class DB_AGVError(models.Model):
    msg_id = models.BigAutoField(primary_key=True)
    timestamp = models.DateTimeField(default= timezone.now)
    car_id = models.IntegerField()
    error_id = models.ForeignKey(DB_Errors, to_field='error_id',  on_delete= models.CASCADE, null= True)
    previous_node = models.IntegerField()
    next_node = models.IntegerField()


