from django.contrib import admin
from .models import DB_AGVData, DB_AGVSpecs, DB_AGVStates

# Register your models here.

admin.site.register(DB_AGVData)
admin.site.register(DB_AGVSpecs)
admin.site.register(DB_AGVStates)
