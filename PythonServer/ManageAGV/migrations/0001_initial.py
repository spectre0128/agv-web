# Generated by Django 4.0 on 2023-02-17 16:51

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='DB_AGVData',
            fields=[
                ('data_id', models.BigAutoField(primary_key=True, serialize=False)),
                ('car_id', models.IntegerField()),
                ('car_state', models.IntegerField()),
                ('car_battery_capacity', models.FloatField()),
                ('car_speed', models.FloatField()),
                ('previous_node', models.IntegerField()),
                ('next_node', models.IntegerField()),
                ('distance', models.FloatField()),
                ('energy_sum', models.IntegerField()),
                ('distance_sum', models.FloatField()),
                ('time_stamp', models.DateTimeField(blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='DB_AGVSpecs',
            fields=[
                ('vehicle_id', models.IntegerField(primary_key=True, serialize=False)),
                ('vehicle_model', models.CharField(blank=True, max_length=255)),
                ('battery_capacity', models.IntegerField(default=0)),
                ('load_capacity', models.IntegerField(default=0)),
                ('travel_speed', models.IntegerField(default=0)),
                ('guidance_type', models.CharField(blank=True, choices=[('OPT', 'Optical Tape'), ('MAG', 'Magnetic Tape'), ('WIR', 'Wire Guided'), ('LZR', 'Laser Guided')], max_length=255)),
                ('load_transfer', models.CharField(blank=True, choices=[('AUTO', 'Automatic'), ('MAN', 'Manual')], max_length=255)),
                ('is_active', models.BooleanField(default=True)),
                ('connected', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='DB_AGVStates',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('state_id', models.IntegerField(default=0)),
                ('state_name', models.CharField(default='None', max_length=16)),
            ],
        ),
    ]
