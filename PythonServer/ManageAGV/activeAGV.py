from django.utils import timezone
import datetime
import time

from ManageAGV.models import DB_AGVData, DB_AGVSpecs

def is_agv_active(carID):
    AGVisActive = DB_AGVSpecs.objects.filter(vehicle_id = carID, is_active = True, connected = True).exists()
    return AGVisActive

def list_active_AGV():
    listOfActiveAGV = []
    AGVisActive = DB_AGVSpecs.objects.all().filter(is_active = True, connected = True).order_by('vehicle_id')
    for eachQuery in AGVisActive:
        listOfActiveAGV.append([eachQuery.vehicle_id, eachQuery.parking_lot])
    return listOfActiveAGV

def check_connect_AGV():
    now = time.mktime(datetime.datetime.now().timetuple)

    listOfActiveAGV = list_active_AGV()
    for eachCar in listOfActiveAGV:
        query = DB_AGVData.objects.all().filter(car_id = eachCar[0]).last()
        dataTime = time.mktime(query.time_stamp.timetuple())
        if (now - dataTime) > 300:
            DB_AGVSpecs.objects.filter(vehicle_id = eachCar[0]).update(connected = False)

# unused functions 

def list_AGV():
    listOfAGV = []
    AGVisActive = DB_AGVSpecs.objects.all().order_by('vehicle_id')
    for eachQuery in AGVisActive:
        listOfAGV.append(eachQuery.vehicle_id)
    return listOfAGV

# def agv_hello():
#     publishMsg('AGVHello', 'Hello')
#     scheduleThread = Thread(target = threaded_hello, args = ())
#     scheduleThread.start()

# def threaded_hello():
#     aAGV = list_AGV()
#     while(True):
#         for eachCar in aAGV:
#             publishMsg('AGVHello/{id}'.format(id = eachCar), 'Hello')
#             sleep(0.5)
#         sleep(10)

def list_available_AGV():
    listOfAvailableAGV = []
    listOfActiveAGV = list_active_AGV()
    for eachCar in listOfActiveAGV:
        AGVisAvailable = DB_AGVData.objects.all().filter(car_state__in = [1, 7], car_id = eachCar).last()
        if AGVisAvailable:
            listOfAvailableAGV.append([AGVisAvailable.car_id, AGVisAvailable.previous_node, AGVisAvailable.next_node])
    return listOfAvailableAGV

def check_connect_AGV():
    listOfActiveAGV = list_active_AGV()
    for eachCar in listOfActiveAGV:
        query = DB_AGVData.objects.all().filter(car_id = eachCar[0], car_state = 11).last()
        if query:
            DB_AGVSpecs.objects.filter(vehicle_id = eachCar[0]).update(connected = False)

    pass

def deactivate_AGV():
    timeNow = timezone.now()
    listOfActiveAGV = list_active_AGV()
    for eachCar in listOfActiveAGV:
        query = DB_AGVData.objects.all().filter(car_id = eachCar).last()
        if query and (timeNow - query.time_stamp).total_seconds() >= 180:
            DB_AGVSpecs.objects.filter(vehicle_id = eachCar).update(is_active = False)

# check_connect_AGV()

# while True:
#     deactivateAGV = deactivate_AGV()