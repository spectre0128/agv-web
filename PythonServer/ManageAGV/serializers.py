from rest_framework import serializers
from .models import DB_AGVSpecs

class AGVSpecsSerializer(serializers.ModelSerializer):
    class Meta:
        model = DB_AGVSpecs
        fields = ('vehicle_id', 'vehicle_model',
                  'battery_capacity', 'load_capacity',
                    'travel_speed', 'guidance_type',
                     'load_transfer', 'is_active')