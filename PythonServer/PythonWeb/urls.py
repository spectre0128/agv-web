"""PythonWeb URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers

import ManageAGV.views as ManageAGVViews
import ManageRequests.views as ManageRequestsViews
import ManageMaterial.views as ManageMaterialViews
import ManageUsers.views as ManageUsersViews

from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

router = routers.DefaultRouter()

router.register(r'agvSpecs', ManageAGVViews.AGVSpecsView, 'Manage AGVs')
router.register(r'orders', ManageRequestsViews.OrderView, 'Manage Orders')
router.register(r'schedules', ManageRequestsViews.ScheduleView, 'Manage Schedules')
router.register(r'materials', ManageMaterialViews.MaterialView, 'Manage Materials')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('Home/',include('Home.urls')),
    path('ManageRequests/',include('ManageRequests.urls')),
    path('ManageAGV/', include('ManageAGV.urls')),

    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('api/user/', ManageUsersViews.UserAPIView.as_view(), name='user'),
    path('api/', include(router.urls)),
    path('api/', include('ManageUsers.urls'))
]
