import paho.mqtt.client as mqtt

from PythonWeb.Decode.decodeData import decodeThis

host = '100.76.157.50'
port = 1883

def subscribeTo():
    client.subscribe("AGVData/#")
    client.subscribe("AGVError/#")
    client.subscribe("AGVHi/#")

def publishMsg(pubTopic, pubPayload):
    client.publish(topic= pubTopic, payload= pubPayload)

def on_publish(client, userdata, mid):
    print("mid: "+str(mid))

def on_subscribe(client, userdata, mid, granted_qos):
    print("Subscribed: "+str(mid)+" "+str(granted_qos))

def on_log(client, userdata, level, string): 
    print(string)

def on_connect(client, userdata, flags, rc):
    print("Connected")
    client.publish("ConAck", payload="Connected", qos=0, retain=False)
    subscribeTo()

def on_message(client, userdata, msg):
    client.publish("MessageAck", payload="Message received", qos=0, retain=False)
    decodeThis(msg.topic, msg.payload)

def on_disconnect(client, userdata,rc=0):
    client.loop_stop()
    
client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
# client.on_publish = on_publish
client.on_subscribe = on_subscribe

client.connect(host, port, 300)

client.loop_start() 