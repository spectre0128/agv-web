from ManageAGV.models import DB_AGVData, DB_AGVError
from ManageRequests.models import DB_Schedule, DB_SimpleOrder

from django.utils import timezone
import json

def insertAGVData(AGVData):
    DB_AGVData.objects.create(car_id = AGVData.carID, 
                              car_state = AGVData.carState, 
                              car_battery_capacity = AGVData.carBatteryCap/100, 
                              car_speed = AGVData.carSpeed/100, 
                              previous_node = AGVData.carPosition.prevNode, 
                              next_node = AGVData.carPosition.nextNode, 
                              distance = AGVData.carPosition.distance/100, 
                              energy_sum = AGVData.energySum, 
                              distance_sum = AGVData.distanceSum/100,
                              time_stamp = timezone.now())
    
def insertAGVError(AGVError):
    DB_AGVError.objects.create(car_id = AGVError.carID,
                               order_number = AGVError.orderNum,
                               error_id = AGVError.errorCode,
                               previous_node = AGVError.prevNode, 
                               next_node = AGVError.nextNode)
    
def insertOrder(Order):
    query = DB_Schedule.objects.filter(order_number = Order.Order, order_date = Order.Date)
    if query:
        query.delete()
    else:
        pass
    DB_Schedule.objects.create(order_number = Order.Order,
                                load_name = Order.Name,
                                load_weight = Order.LoadWeight,
                                order_date = Order.Date,
                                car_id = Order.get_car_id(),
                                energy_sum = Order.TotalEnergy,
                                distance_sum = Order.get_total_distance(),
                                start_time = Order.TimeStart,
                                end_time = Order.TimeEnd,
                                from_node = Order.Inbound,
                                to_node = Order.Outbound,
                                control_signal = json.dumps(Order.list_control_signal()))
    
    DB_SimpleOrder.objects.filter(order_number = Order.Order).update(scheduled_st = True)
    
    
